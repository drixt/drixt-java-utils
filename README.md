<h1>DRIXT - COMMON JAVA UTILS - v1.0.3</h1>

Some utilities to improve and save time working with java.
---

I will document every class with their functions on this readme,
I promise, but for now, you can check the files by yourself.
;)
Every Extension or Utility function is already documented.

<h3>Building and Releasing</h3>
> $ mvn clean package deploy
