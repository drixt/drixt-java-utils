package com.drixt.utils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * Mergeablify a pojo with a superpower to copy property values of another pojos.
 * It's very experimental.
 * Check the side effects.
 * <p>
 * Rodrigo
 */
public interface Mergeable extends Serializable {


  /**
   *  Recursive method to get all fields declared in a class and its superclasses.
   *  It's a workaround to get all fields declared in a class.
   */
  private void accumulateSuperclassDeclaredFields(Class<?> clazz, List<Field> accumulator) {
    Class<?> superclass = clazz.getSuperclass();
    if (superclass != null) {
      accumulateSuperclassDeclaredFields(superclass, accumulator);
      Collections.addAll(accumulator, superclass.getDeclaredFields());
    }
  }

  /**
   * Get all fields declared in a class and its superclasses.
   * It's a workaround to get all fields declared in a class.
   */
  private List<Field> getAllDeclaredFields(Class<?> clazz) {
    var allFields = new ArrayList<>(Stream.of(clazz.getDeclaredFields()).toList());
    accumulateSuperclassDeclaredFields(clazz, allFields);

    return allFields;
  }

  /**
   * Fill current object fields with new object values, ignoring new NULLs. Old values are overwritten.
   *
   *  @param newObject    Any object type
   * @param ignoreFields List os field names to ignore. The list can be null in case of no ignoring.
   */
  default void merge(Object newObject, List<String> ignoreFields) {
    var allTargetDeclaredFields = getAllDeclaredFields(this.getClass());

    for (Field field : allTargetDeclaredFields) {
      if (ignoreFields != null && ignoreFields.contains(field.getName()))
        continue;

      var allSourceDeclaredFields = getAllDeclaredFields(newObject.getClass());

      for (Field newField : allSourceDeclaredFields) {

        if (field.getName().equals(newField.getName()) && field.getType().equals(newField.getType())) {
          try {
            field.setAccessible(true);
            newField.setAccessible(true);

            if (newField.get(newObject) != null && !newField.get(newObject).equals(field.get(this)))
              field.set(this, newField.get(newObject));

            break;

          } catch (IllegalAccessException ignore) {
            // Field update exception on final modifier and other cases.
          }
        }
      }
    }
  }

  /**
   * Fill current object fields with new object values, ignoring new NULLs. Old values are overwritten.
   *
   * @param newObject Same type object with new values.
   */
  default void merge(Object newObject) {
    this.merge(newObject, null);
  }
}

