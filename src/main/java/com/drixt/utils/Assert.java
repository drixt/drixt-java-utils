package com.drixt.utils;

import java.util.Collection;
import java.util.Map;


/**
 * Utilty assert class with some usuful way to throw exceptions
 */
public abstract class Assert {

  /*
   * IsTrue and IsFalse
   */

  /**
   * Expect that condition to be true, otherwise, trow an exception.
   *
   * @param condition     - Evaluating condition
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isTrue(boolean condition, Object message, Object... messageParams) {
    if (!condition)
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to be false, otherwise, trow an exception
   *
   * @param condition     - Evaluating condition
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isFalse(boolean condition, Object message, Object... messageParams) {
    if (condition)
      throw new AssertException(message, messageParams);
  }


  /*
   * IsNull and IsNotNull
   */

  /**
   * Expect that condition to be null, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isNull(Object value, Object message, Object... messageParams) {
    if (value != null)
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to be not null, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isNotNull(Object value, Object message, Object... messageParams) {
    if (value == null)
      throw new AssertException(message, messageParams);
  }


  /*
   * IsEmpty
   */

  /**
   * Expect that condition to be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isEmpty(String value, Object message, Object... messageParams) {
    if (value != null && !value.trim().isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isEmpty(Collection<?> value, Object message, Object... messageParams) {
    if (value != null && !value.isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isEmpty(Map<?, ?> value, Object message, Object... messageParams) {
    if (value != null && !value.isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static <V> void isEmpty(V[] value, Object message, Object... messageParams) {
    if (value != null && value.length > 0)
      throw new AssertException(message, messageParams);
  }


  /*
   * IsNotEmpty
   */

  /**
   * Expect that condition to not be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isNotEmpty(String value, Object message, Object... messageParams) {
    if (value == null || value.trim().isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to not be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isNotEmpty(Collection<?> value, Object message, Object... messageParams) {
    isNotNull(value, message, messageParams);
    if (value.isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to not be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static void isNotEmpty(Map<?, ?> value, Object message, Object... messageParams) {
    isNotNull(value, message, messageParams);
    if (value.isEmpty())
      throw new AssertException(message, messageParams);
  }

  /**
   * Expect that condition to not be empty, otherwise, trow an exception.
   *
   * @param value         - The value
   * @param message       - String of error
   * @param messageParams - Param values of message variables - optional
   */
  public static <V> void isNotEmpty(V[] value, Object message, Object... messageParams) {
    isNotNull(value, message, messageParams);
    if (value.length == 0)
      throw new AssertException(message, messageParams);
  }
}
