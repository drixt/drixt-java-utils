package com.drixt.utils;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import lombok.RequiredArgsConstructor;


/**
 * Utility class to help internationalize messages (I18N)
 *
 * @author rodrigo tessarollo
 */
@RequiredArgsConstructor
public class I18N implements Serializable {

  public final List<Locale> supportedLocales;

  public final List<String> baseNames;

  public String getString(Object key, Object... params) {
    return getString(null, key instanceof Enum<?> ? ((Enum<?>) key).name() : key.toString(), params);
  }

  public String getString(Locale locale, Object key, Object... params) {
    List<ResourceBundle> bundles = getBundles(locale);

    if (key == null)
      throw new RuntimeException("I18N: Message key must not be null");

    if (params != null && params.length > 0) {
      List<String> resolvedParams = new ArrayList<>(params.length);
      for (Object param : params) {
        if (param == null)
          throw new RuntimeException("I18N: Message parameters must not be null. Current message key: " + key);

        resolvedParams.add(param instanceof Enum
          ? resolveKey(bundles, (Enum<?>) param)
          : resolveKey(bundles, param.toString()));
      }

      return new MessageFormat(resolveKey(bundles, key.toString()))
        .format(resolvedParams.toArray());

    } else {
      return resolveKey(bundles, key.toString());
    }
  }

  /*
   * Private members
   */

  private List<ResourceBundle> getBundles(Locale locale) {
    locale = resolveLocale(locale);

    var bundles = new ArrayList<ResourceBundle>();

    for (String bn : baseNames) {
      bundles.add(ResourceBundle.getBundle(bn, locale));
    }

    return bundles;
  }

  private Locale resolveLocale(Locale locale) {
    if (locale == null) {
      locale = LocaleContextHolder.getLocale();
    }

    return supportedLocales.contains(locale) ? locale : supportedLocales.get(0);
  }

  private String resolveKey(List<ResourceBundle> bundles, String key) {
    for (ResourceBundle bundle : bundles)
      if (bundle.containsKey(key))
        return bundle.getString(key);

      else if (bundle.containsKey(key.toUpperCase()))
        return bundle.getString(key.toUpperCase());

    return key;
  }

  private String resolveKey(List<ResourceBundle> bundles, Enum<?> key) {
    String resolvedString = null;

    for (ResourceBundle bundle : bundles)
      if (bundle.containsKey(key.name())) {
        resolvedString = bundle.getString(key.name());
        break;
      }

    if (StringUtils.isEmpty(resolvedString))
      resolvedString = this.resolveKey(bundles, key.toString());

    return StringUtils.isNotEmpty(resolvedString) ? resolvedString : key.name();
  }
}
