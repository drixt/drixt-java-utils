package com.drixt.utils;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 * Exception class to trhow assert exceptions
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AssertException extends RuntimeException {

  /**
   * Error code
   */
  private Integer code;

  /**
   * Message description
   */
  private Object errorMessage;

  /**
   * Customizable params
   */
  private List<Object> params;


  /**
   * Create the excpetion with all data
   *
   * @param messageCode - the nessage
   * @param message     - the nessage
   * @param params      - the params
   */
  public AssertException(int messageCode, Object message, Object... params) {
    this.code = messageCode;
    this.errorMessage = message;

    if (params != null && params.length > 0 && Stream.of(params).anyMatch(Objects::nonNull)) {
      this.params = Stream.of(params).filter(Objects::nonNull).collect(Collectors.toList());
    }
  }

  /**
   * Create the excpetion with only basica data
   *
   * @param message - the nessage
   * @param params  - the params
   */
  public AssertException(Object message, Object... params) {
    this(400, message, params);
  }
}
